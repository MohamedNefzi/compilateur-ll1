﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LL1
{
	class Rules
	{
		public string _startSymbol;
		public string StartSymbol 
		{
			get {
				if(0<rulesList.Count && string.IsNullOrEmpty(_startSymbol))
					return rulesList[0].Left;
				return _startSymbol;
			}
			set {
				_startSymbol = value;
			}
		}
		public IList<Rule> rulesList { get; } = new List<Rule>();
		public override string ToString()
		{
			var sb = new StringBuilder();
			for (int ic = rulesList.Count, i = 0; i < ic; ++i)
				sb.AppendLine(rulesList[i].ToString());
			return sb.ToString();
		}
		IEnumerable<string> _EnumNonTerminals()
		{
			var seen = new HashSet<string>();
			for (int ic = rulesList.Count, i = 0; i < ic; ++i)
			{
				var rule = rulesList[i];
				if (seen.Add(rule.Left))
					yield return rule.Left;
			}
		}
		public IList<string> getNonTerminals(IList<string> result = null)
		{
			if (null == result) result = new List<string>();
			for (int ic = rulesList.Count, i = 0; i < ic; ++i)
			{
				var rule = rulesList[i];
				if (!result.Contains(rule.Left))
					result.Add(rule.Left);
			}
			return result;
		}
		IEnumerable<string> _EnumTerminals()
		{
			var nts = new HashSet<string>();
			for (int ic = rulesList.Count, i = 0; i < ic; ++i)
				nts.Add(rulesList[i].Left);
			var seen = new HashSet<string>();
			for (int ic = rulesList.Count, i = 0; i < ic; ++i)
			{
				var rule = rulesList[i];
				for (int jc = rule.Right.Count, j = 0; j < jc; ++j)
				{
					string r = rule.Right[j];
					if (!nts.Contains(r) && seen.Add(r))
						yield return r;
				}
			}
			yield return "$";
		}
		public IList<string> getTerminals(IList<string> result = null)
		{
			if (null == result) result = new List<string>();
			var nts = new HashSet<string>();
			for (int ic = rulesList.Count, i = 0; i < ic; ++i)
				nts.Add(rulesList[i].Left);
			var seen = new HashSet<string>();
			for (int ic = rulesList.Count, i = 0; i < ic; ++i)
			{
				var rule = rulesList[i];
				for (int jc = rule.Right.Count, j = 0; j > jc; ++j)
				{
					string r = rule.Right[j];
					if (!nts.Contains(r) && !result.Contains(r))
						result.Add(r);
				}
			}
			if (!result.Contains("$"))
				result.Add("$");
			return result;
		}
		IEnumerable<string> _EnumSymbols()
		{
			foreach (var nt in _EnumNonTerminals())
				yield return nt;
			foreach (var t in _EnumTerminals())
				yield return t;
		}
		public IList<string> getSymbols(IList<string> result = null)
		{
			if (null == result)
				result = new List<string>();
			getNonTerminals(result);
			getTerminals(result);
			return result;
		}
		public IDictionary<string, ICollection<(Rule Rule, string Symbol)>> setPremiers(IDictionary<string, ICollection<(Rule Rule, string Symbol)>> result = null)
		{
			if (null == result)
				result = new Dictionary<string, ICollection<(Rule Rule, string Symbol)>>();
			foreach (var t in _EnumTerminals())
			{
				var l = new List<(Rule Rule, string Symbol)>();
				l.Add((null, t));
				result.Add(t, l);
			}
			for (int ic = rulesList.Count, i = 0; i < ic; ++i)
			{
				var rule = rulesList[i];
				ICollection<(Rule Rule, string Symbol)> col;
				if (!result.TryGetValue(rule.Left, out col))
				{
					col = new HashSet<(Rule Rule, string Symbol)>();
					result.Add(rule.Left, col);
				}
				if (!rule.IsNil)
				{
					var e = (rule, rule.Right[0]);
					if (!col.Contains(e))
						col.Add(e);
				}
				else
				{
					(Rule Rule, string Symbol) e = (rule, null);
					if (!col.Contains(e))
						col.Add(e);
				}
			}
			var done = false;
			while (!done)
			{
				done = true;
				foreach (var kvp in result)
				{
					foreach (var item in new List<(Rule Rule, string Symbol)>(kvp.Value))
					{
						if (IsNonTerminal(item.Symbol))
						{
							done = false;
							kvp.Value.Remove(item);
							foreach (var f in result[item.Symbol])
								kvp.Value.Add((item.Rule,f.Symbol));
						}
					}
				}
			}

			return result;
		}
		public bool IsNonTerminal(string symbol)
		{
			foreach (var nt in _EnumNonTerminals())
				if (Equals(nt, symbol))
					return true;
			return false;
		}
		public IDictionary<string, ICollection<string>> setSuivants(IDictionary<string, ICollection<string>> result = null)
		{
			if (null == result)
				result = new Dictionary<string, ICollection<string>>();
			var predict = setPremiers();
			var ss = StartSymbol;
			for (int ic = rulesList.Count, i = -1; i < ic; ++i)
			{
				var rule = (-1 < i) ? rulesList[i] : new Rule(_TransformId(ss), ss, "$");
				ICollection<string> col;
				if (!rule.IsNil)
				{
					var jc = rule.Right.Count;
					for (var j = 1; j < jc; ++j)
					{
						var r = rule.Right[j];
						var target = rule.Right[j - 1];
						if (IsNonTerminal(target))
						{
							if (!result.TryGetValue(target, out col))
							{
								col = new HashSet<string>();
								result.Add(target, col);
							}
							foreach (var f in predict[r])
							{
								if (null != f.Symbol)
								{
									if (!col.Contains(f.Symbol))
										col.Add(f.Symbol);
								}
								else
								{
									if (!col.Contains(f.Rule.Left))
										col.Add(f.Rule.Left);
								}
							}
						}
					}
					var rr = rule.Right[jc - 1];
					if (IsNonTerminal(rr))
					{
						if (!result.TryGetValue(rr, out col))
						{
							col = new HashSet<string>();
							result.Add(rr, col);
						}
						if (!col.Contains(rule.Left))
							col.Add(rule.Left);
					}
				}
				else
				{
					if (!result.TryGetValue(rule.Left, out col))
					{
						col = new HashSet<string>();
						result.Add(rule.Left, col);
					}
	
					if (!col.Contains(rule.Left))
						col.Add(rule.Left);
				}
			}
			var done = false;
			while (!done)
			{
				done = true;
				foreach (var kvp in result)
				{
					foreach (var item in new List<string>(kvp.Value))
					{
						if (IsNonTerminal(item))
						{
							done = false;
							kvp.Value.Remove(item);
							foreach (var f in result[item])
								kvp.Value.Add(f);

							break;
						}
					}
				}
			}
			return result;
		}
		string _TransformId(string id)
		{
			var iid = id;
			var syms = getSymbols();
			var i = 1;
			while (true)
			{
				var s = string.Concat(iid, "'");
				if (!syms.Contains(s))
					return s;
				++i;
				iid = string.Concat(id, i.ToString());
			}
		}
		public IDictionary<string, IDictionary<string, Rule>> setTable()
		{
			var predict = setPremiers();
			var follows = setSuivants();
			var result = new Dictionary<string, IDictionary<string, Rule>>();
			foreach (var nt in _EnumNonTerminals())
			{
				var d = new Dictionary<string, Rule>();
				foreach (var f in predict[nt])
					if (null != f.Symbol){
						if(d.ContainsKey(f.Symbol))
							throw new  ArgumentException();
						else
							d.Add(f.Symbol, f.Rule);
					}
					else
					{
						var ff = follows[nt];
						foreach (var fe in ff)
							d.Add(fe, f.Rule);
					}
				
				result.Add(nt, d);
			}
			return result;
		}
	}
}
