﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LL1
{
	partial class Program
	{
		static void Main(string[] args)
		{


			Rules rules = new Rules();
			// Exemple Grammaire n'est pas LL1
			
			rules.rulesList.Add(new Rule("S", "a", "A", "b"));
			rules.rulesList.Add(new Rule("A", "c", "d"));
			rules.rulesList.Add(new Rule("A", "c"));
			
			// Exemple Grammaire LL1

			/*
			rules.rulesList.Add(new Rule("E", "T", "E'"));
			rules.rulesList.Add(new Rule("E'", "+", "T", "E'"));
			rules.rulesList.Add(new Rule("E'"));
			rules.rulesList.Add(new Rule("T", "F", "T'"));
			rules.rulesList.Add(new Rule("T'", "*", "F", "T'"));
			rules.rulesList.Add(new Rule("T'"));
			rules.rulesList.Add(new Rule("F", "(", "E", ")"));
			rules.rulesList.Add(new Rule("F", "int"));
			
			*/
			
			Console.WriteLine("\n -------------- Grammaire ----------------");
			Console.WriteLine(rules);
			Console.WriteLine("L'axiome de cette grammaire est : "+rules.StartSymbol);
			IList<string> result = rules.getNonTerminals();
			Console.WriteLine("Non Terminals : ");
			foreach (var s in result)
				Console.Write(" "+ s);
			IDictionary<string, ICollection<(Rule Rule, string Symbol)>> Premiers = rules.setPremiers();
			Console.WriteLine("\n -------------- Ensemble des Premiers ----------------");
			foreach (var  p in Premiers){
				if (String.Compare(p.Key,"$") != 0){
					Console.Write("Premier("+p.Key+"): { ");	
					foreach (var s in p.Value){
						if (String.Compare(s.Symbol,null) == 0)
							Console.Write("Eps");
						else 
							Console.Write(s.Symbol+" ");
					}
					Console.WriteLine("}");
				}
			}
			Console.WriteLine("\n -------------- Ensemble des Suivants ----------------");
			IDictionary<string, ICollection<string>> suivants = rules.setSuivants();
			foreach (var  s in suivants){
				Console.Write("Suivant("+s.Key+") : { ");	
				foreach (var x in s.Value)
					Console.Write(x+" ");
				Console.WriteLine("}");
			}
			Console.WriteLine("\n -------------- Table d'analyse ----------------");
			try
			{		
				IDictionary<string, IDictionary<string, Rule>> table = rules.setTable();
				Console.WriteLine("Cette grammaire est du type LL(1)");	
				foreach (var  s in table){
					Console.Write("\n"+ s.Key+" | ");
					foreach (var x in s.Value){
						Console.Write(x.Key+" : "+ x.Value+" | ");	
					}
				}
			}
			catch (ArgumentException )
			{
				Console.WriteLine("Oops, cette grammaire n'est pas du type LL(1), il existe une case dans le table d'analyse définie de façon multiple.");
			}
			Console.WriteLine("");
		}
	}
}
