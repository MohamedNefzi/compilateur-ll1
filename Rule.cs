﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LL1
{
	class Rule : IEquatable<Rule>
	{
		public string Left { get; set; } = null;
		public IList<string> Right { get; } = new List<string>();
		public Rule(string left,IEnumerable<string> right) { Left = left; if(null!=Right) Right = new List<string>(right); }
		public Rule(string left, params string[] right) : this(left,(IEnumerable<string>)right) { }

		public Rule() { }
		
		public bool IsNil { get { return 0==Right.Count; } }

		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.Append(Left ?? "");
			sb.Append(" ->");
			var ic = Right.Count;
			if(ic == 0){
				sb.Append(" ");
				sb.Append("Eps");
			}
			else
				for(var i = 0;i < ic;++i)
				{
					sb.Append(" ");
					sb.Append(Right[i]);
				}
			return sb.ToString();
		}
		public bool Equals(Rule rhs)
		{
			if (ReferenceEquals(this, rhs)) return true;
			if (ReferenceEquals(null,rhs)) return false;
			if(Equals(Left,rhs.Left))
			{
				if(Right.Count == rhs.Right.Count)
				{
					for(int ic = Right.Count,i=0;i<ic;++i)
						if (!Equals(Right[i], rhs.Right[i]))
							return false;
					return true;
				}
			}
			return false;
		}
		public override bool Equals(object obj)
			=> Equals(obj as Rule);
		public override int GetHashCode()
		{
			var result = 0;
			if (null != Left)
				result ^= Left.GetHashCode();
			for (int ic = Right.Count, i = 0; i < ic; ++i) {
				var r = Right[i];
				if (null != r) result ^= r.GetHashCode();
			}
			return result;
		}
		public static bool operator==(Rule lhs,Rule rhs)
		{
			if (ReferenceEquals(lhs, rhs))
				return true;
			if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
				return false;
			return lhs.Equals(rhs);
		}
		public static bool operator !=(Rule lhs, Rule rhs)
		{
			if (ReferenceEquals(lhs, rhs))
				return false;
			if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
				return true;
			return !lhs.Equals(rhs);
		}
	}
}
